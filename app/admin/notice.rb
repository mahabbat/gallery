ActiveAdmin.register Notice do
  permit_params :user_id, :score, :text

  form do |f|
    f.inputs do
      f.input :user
      f.input :score
      f.input :text
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :user do |notice|
      link_to notice.user, admin_notices_path(notice)
    end
    column :score
    actions
  end

  show do
    attributes_table do
      row :user
      row :text
      row :score
    end
    active_admin_comments
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
