ActiveAdmin.register User do
  permit_params :name, :email, :password, :password_confirmation

  form do |f|
    f.inputs do
      f.input :name
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :name do |user|
      link_to user.name, admin_users_path(user)
    end

    actions
  end

  show do
    attributes_table do
      row :name
    end
    active_admin_comments
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
