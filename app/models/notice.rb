class Notice < ActiveRecord::Base
  belongs_to :user
  belongs_to :photo
  belongs_to :admin_user
end
