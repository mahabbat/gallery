class AdminUser < ActiveRecord::Base
  has_many :users
  has_many :photos
  has_many :notices
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :rememberable, :trackable
end