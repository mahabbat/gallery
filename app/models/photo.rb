class Photo < ActiveRecord::Base
  belongs_to :user
  belongs_to :admin_user
  has_many :notices

  has_attached_file :image,
                    styles: { medium: '300x300>', thumb: '100x100>'},
                    default_url: '/images/:style/missing.png'
  validates_attachment_content_type :image,
                                    content_type: ['image/jpeg', 'image/png']
end
