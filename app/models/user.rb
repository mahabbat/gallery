class User < ActiveRecord::Base
  belongs_to :admin_user
  has_many :photos
  has_many :notices

  validates :name, presence: true
  validates :email, presence: true
  validates :password, presence: true

end
