class CreateNotices < ActiveRecord::Migration
  def change
    create_table :notices do |t|
      t.references :user, index: true, foreign_key: true
      t.decimal :score
      t.text :text
      t.references :photo, index: true, foreign_key: true
      t.references :admin_user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
